#!/bin/sh
cd apt-repo
mkdir -p dists/focal/main/binary-amd64/
dpkg-scanpackages --arch amd64 pool/ > dists/focal/main/binary-amd64/Packages
cat dists/focal/main/binary-amd64/Packages | gzip -9 > dists/focal/main/binary-amd64/Packages.gz
../generate-release.sh > dists/focal/Release
#cat ~/example/apt-repo/dists/focal/Release | gpg --default-key example -abs > ~/example/apt-repo/dists/focal/Release.gpg
